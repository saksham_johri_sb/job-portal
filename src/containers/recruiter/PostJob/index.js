import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router';
import './style.scss';
import homeIcon from '../../../assets/images/homeIcon.png';
import { postJobValidation } from '../../../utils/validations';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import { postJob } from '../../../redux/action/recruiter/postJob';
import loadingImage from '../../../assets/images/loading.gif';

const PostJob = props => {
    const { history = {} } = props || {};

    const dispatch = useDispatch();

    const {
        postJob: { isLoading, isSubmited, error: apiError },
    } = useSelector(state => state);

    const TITLE = 'Job Portal - Post Job';

    const [data, setData] = useState({
        title: '',
        description: '',
        location: '',
    });

    const [error, setError] = useState(false);

    const [titleError, setTitleError] = useState(false);
    const [descriptionError, setDescriptionError] = useState(false);
    const [locationError, setLocationError] = useState(false);

    useEffect(() => {
        if (isSubmited) {
            setData({
                title: '',
                description: '',
                location: '',
            });
        }
    }, [isSubmited]);

    useEffect(() => {
        if (apiError) setError(apiError);
    }, [apiError]);

    const handelOnChange = ({ name, value }) => {
        setData({
            ...data,
            [name]: value,
        });
    };

    const onSubmit = () => {
        const checkValidations = postJobValidation({
            ...data,
            setTitleError,
            setDescriptionError,
            setLocationError,
        });

        if (checkValidations) {
            dispatch(postJob(data));
        }
    };

    if (isLoading)
        return (
            <div className='loading'>
                <img src={loadingImage} alt='Loading...' height={40} />
            </div>
        );

    return (
        <>
            <Helmet>
                <title>{TITLE}</title>
            </Helmet>

            <div className='postAJob'>
                <div className='post-form-background' />

                <div className='postJobConatainer come-to-front'>
                    <div
                        className='homeIcon'
                        onClick={() => history?.push('/recruiter/dashboard')}
                    >
                        <img src={homeIcon} alt='home' />
                        <p>{'Home > Post a Job'}</p>
                    </div>

                    <div className='formContainer'>
                        <div className='formInnerContainer'>
                            <div className='postAJobTitle'>
                                <p>Post a Job</p>
                            </div>

                            <div className='inputFields'>
                                <div className='inputFieldClild'>
                                    <label>
                                        Job title
                                        <span className='required'>*</span>
                                    </label>
                                    <input
                                    autoComplete='off'
                                        placeholder='Enter job title'
                                        type='text'
                                        value={data?.title}
                                        name='title'
                                        onChange={e =>
                                            handelOnChange(e?.target)
                                        }
                                        onFocus={() => setTitleError(false)}
                                        style={{
                                            borderColor: titleError
                                                ? '#FF333380'
                                                : null,
                                        }}
                                    />
                                    {titleError ? (
                                        <p className='postAJobError'>
                                            {titleError}
                                        </p>
                                    ) : (
                                        void 0
                                    )}
                                </div>

                                <div className='inputFieldClild'>
                                    <label>
                                        Description
                                        <span className='required'>*</span>
                                    </label>
                                    <textarea
                                        placeholder='Enter job description'
                                        type='textarea'
                                        value={data?.description}
                                        name='description'
                                        onChange={e =>
                                            handelOnChange(e?.target)
                                        }
                                        onFocus={() =>
                                            setDescriptionError(false)
                                        }
                                        style={{
                                            borderColor: descriptionError
                                                ? '#FF333380'
                                                : null,
                                        }}
                                    />
                                    {descriptionError ? (
                                        <p className='postAJobError'>
                                            {descriptionError}
                                        </p>
                                    ) : (
                                        void 0
                                    )}
                                </div>

                                <div className='inputFieldClild'>
                                    <label>
                                        Location
                                        <span className='required'>*</span>
                                    </label>
                                    <input
                                    autoComplete='off'
                                        placeholder='Enter location'
                                        type='text'
                                        value={data?.location}
                                        name='location'
                                        onChange={e =>
                                            handelOnChange(e?.target)
                                        }
                                        onFocus={() => setLocationError(false)}
                                        style={{
                                            borderColor: locationError
                                                ? '#FF333380'
                                                : null,
                                        }}
                                    />

                                    {locationError ? (
                                        <p className='postAJobError'>
                                            {locationError}
                                        </p>
                                    ) : (
                                        void 0
                                    )}

                                    {error ? (
                                        <p className='postAJobError'>{error}</p>
                                    ) : (
                                        void 0
                                    )}
                                </div>
                            </div>

                            <div className='btnContainer'>
                                <div className='btn' onClick={onSubmit}>
                                    <p>Post</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default withRouter(PostJob);

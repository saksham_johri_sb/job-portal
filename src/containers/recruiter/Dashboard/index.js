import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { withRouter } from 'react-router';
import Modal from './modal';
import Dashboard from '../../../components/dashboard';
import { useDispatch, useSelector } from 'react-redux';
import { getPostedJobs } from '../../../redux/action/recruiter/getPostedJobs';
import loadingImage from '../../../assets/images/loading.gif';

const RecruiterDashboard = props => {
    const {
        history = {},
        forceHideModal,
        setForceHideModal = () => {},
    } = props || {};

    const dispatch = useDispatch();

    const {
        postedJobs: { postedJobs, isLoading },
    } = useSelector(state => state);

    const TITLE = 'Job Portal - Dashboard';

    const [jobsPosted, setJobsPosted] = useState([]);

    const [selectedJobId, setSelectedJobId] = useState('');

    const [openModal, setOpenModal] = useState(false);

    useEffect(() => {
        dispatch(getPostedJobs());

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        setJobsPosted(postedJobs);
    }, [postedJobs]);

    if (isLoading)
        return (
            <div className='loading'>
                <img src={loadingImage} alt='Loading...' height={40} />
            </div>
        );

    return (
        <>
            <Helmet>
                <title>{TITLE}</title>
            </Helmet>

            <Modal
                id={selectedJobId}
                visible={openModal}
                setVisible={setOpenModal}
            />

            <Dashboard
                array={jobsPosted}
                cardText='View Applications'
                onClick={id => {
                    setSelectedJobId(id);
                    setOpenModal(true);
                    setForceHideModal(!forceHideModal);
                }}
                homeText='Home'
                homeOnClick={() => history.push('/recruiter/dashboard')}
                heading='Jobs posted by you'
                noCardText='Your posted jobs will show here!'
                noCardBtnText='Post a Job'
                noCardOnClick={() => history?.push('/recruiter/post-job')}
            />
        </>
    );
};

export default withRouter(RecruiterDashboard);

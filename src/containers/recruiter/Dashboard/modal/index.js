import React, { useState, useEffect } from 'react';
import './style.scss';
import closeIcon from '../../../../assets/images/closeIcon.png';
import cvImage from '../../../../assets/images/cvImage.png';
import { useDispatch, useSelector } from 'react-redux';
import { getJobApplicants } from '../../../../redux/action/recruiter/getJobApplicants';

const Modal = props => {
    const { id = null, visible = false, setVisible = () => {} } = props || {};

    const dispatch = useDispatch();

    const {
        applicants: { applicants },
    } = useSelector(state => state);

    const [applicantList, setApplicantList] = useState([]);

    const applicantCard = ({ item, index, length }) => {
        const { email, name, skills = false } = item || {};
        return (
            <div
                className='applicant'
                key={index?.toString()}
                // style={{ height: length < 3 ? '13rem' : 'auto' }}
            >
                <div className='applicantDetail'>
                    <div className='profilePicture'>
                        <div>
                            <p>{name?.[0]?.toUpperCase()}</p>
                        </div>
                    </div>

                    <div className='otherDetails'>
                        <p className='name'>{name}</p>
                        <p className='email'>{email}</p>
                    </div>
                </div>

                {skills ? (
                    <div className='skills'>
                        <p className='skillTitle'>Skills</p>
                        <p className='skillList'> {skills} </p>
                        <span className='skills-tooltip'>{skills}</span>
                    </div>
                ) : (
                    void 0
                )}
            </div>
        );
    };

    useEffect(() => {
        if (id) {
            dispatch(getJobApplicants(id));
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id]);

    useEffect(() => {
        setApplicantList(applicants);
    }, [applicants]);

    return (
        <div
            className='modalWindow'
            style={{
                visibility: visible ? 'visible' : 'hidden',
                opacity: visible ? 1 : 0,
                pointerEvents: 'auto',
            }}
        >
            <div className='modalContainer'>
                <div className='modalHeader'>
                    <p>Applicants for this job</p>

                    <p className='modalClose' onClick={() => setVisible(false)}>
                        <img src={closeIcon} alt='close' />
                    </p>
                </div>

                <div className='modalBody'>
                    <p className='numberOfApplications'>
                        {applicantList?.length} applications
                    </p>

                    {applicantList?.length ? (
                        <div className='applicationList notZero'>
                            {applicantList?.map((item, index, array) =>
                                applicantCard({
                                    item,
                                    index,
                                    length: array?.length,
                                })
                            )}
                        </div>
                    ) : (
                        <div className='applicationList zero'>
                            <img src={cvImage} alt='' />
                            <p>No applications available!</p>
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
};

export default Modal;

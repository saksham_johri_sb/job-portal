import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import { getPasswordToken } from '../../redux/action/forgotPasssword';
import { forgotPasswordValidation } from '../../utils/validations';
import './style.scss';
import loadingImage from '../../assets/images/loading.gif';

const ForgotPassword = props => {
    const { history = {} } = props || {};

    const dispatch = useDispatch();

    const { forgotPassword } = useSelector(state => state);
    const {
        token,
        error: apiError,
        isLoading,
        isTokenVerified,
    } = forgotPassword || {};

    useEffect(() => {
        if (apiError && email) {
            if (apiError === 'Request failed with status code 404') {
                setError('Email address does not exist');
                return;
            }

            setError(apiError);
            return;
        }

        if (token && !isTokenVerified) {
            history.push('/reset-password');
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [forgotPassword]);

    const TITLE = 'Job Portal - Forgot Password';

    const [email, setEmail] = useState('');
    const [error, setError] = useState(false);

    const onSubmit = () => {
        const checkValidations = forgotPasswordValidation({ email, setError });
        if (checkValidations) {
            dispatch(getPasswordToken(email));
        }
    };

    if (isLoading)
        return (
            <div className='loading'>
                <img src={loadingImage} alt='Loading...' height={40} />
            </div>
        );

    return (
        <>
            <Helmet>
                <title>{TITLE}</title>
            </Helmet>

            <div className='forgotPassword'>
                <div className='forgot-form-background' />

                <div className='forgotPasswordContainer'>
                    <div className='forgotPasswordInnerContainer come-to-front'>
                        <div className='forgotPasswordTitle'>
                            <p>
                                <b>Forgot your password?</b>
                            </p>
                        </div>

                        <p className='forgotPasswordDesc'>
                            Enter the email associated with your account and
                            we’ll send you instructions to reset your password.
                        </p>

                        <div className='inputFields'>
                            <div className='inputFieldClild'>
                                <label>Email address</label>
                                <input
                                    autoComplete='off'
                                    placeholder='Enter your email'
                                    type='email'
                                    value={email}
                                    name='email'
                                    onChange={e => setEmail(e.target.value)}
                                    onFocus={() => setError(false)}
                                    style={{
                                        borderColor: error ? '#FF333380' : null,
                                    }}
                                />

                                {error ? (
                                    <p className='forgotPasswordError'>
                                        {error}
                                    </p>
                                ) : (
                                    void 0
                                )}
                            </div>
                        </div>

                        <div className='btnContainer'>
                            <div className='btn' onClick={onSubmit}>
                                <p>Submit</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default withRouter(ForgotPassword);

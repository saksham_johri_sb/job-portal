import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { withRouter } from 'react-router';
import { loginValidation } from '../../utils/validations';
import './style.scss';
import { signIn } from '../../redux/action/auth';
import { useDispatch, useSelector } from 'react-redux';
import loadingImage from '../../assets/images/loading.gif';

const SignIn = props => {
    const { history = {} } = props || {};

    const dispatch = useDispatch();
    const { authentication } = useSelector(state => state);
    const { isLoading } = authentication || {};

    useEffect(() => {
        const { userData: { userRole } = {}, signInError } =
            authentication || {};

        if (signInError && data?.email && data?.password) {
            if (signInError === 'Request failed with status code 401') {
                setError('Invalid Email / Password');
                return;
            }

            setError(signInError);
            return;
        }

        if (userRole === 0 && localStorage?.getItem('userRole')) {
            history.push('/recruiter/dashboard');
        } else if (userRole === 1 && localStorage?.getItem('userRole')) {
            history.push('/candidate/dashboard');
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [authentication]);

    const TITLE = 'Job Portal - Login';

    const [data, setData] = useState({
        email: '',
        password: '',
    });

    const handelOnChange = ({ name, value }) => {
        setData({
            ...data,
            [name]: value,
        });
    };

    const [error, setError] = useState(false);

    const onSubmit = () => {
        const checkValidations = loginValidation({ ...data, setError });

        if (checkValidations) {
            dispatch(signIn({ ...data }));
        }
    };

    if (isLoading)
        return (
            <div className='loading'>
                <img src={loadingImage} alt='Loading...' height={40} />
            </div>
        );

    return (
        <>
            <Helmet>
                <title>{TITLE}</title>
            </Helmet>

            <div className='login'>
                <div className='login-form-background' />

                <div className='loginContainer'>
                    <div className='loginInnerContainer come-to-front'>
                        <div className='loginTitle'>
                            <p>Login</p>
                        </div>

                        <div className='inputFields'>
                            <div className='inputFieldClild'>
                                <label>Email address</label>
                                <input
                                    autoComplete='off'
                                    placeholder='Enter your email'
                                    type='email'
                                    value={data?.email}
                                    name='email'
                                    onChange={e => handelOnChange(e?.target)}
                                    onFocus={() => setError(false)}
                                    style={{
                                        borderColor: error ? '#FF333380' : null,
                                    }}
                                />
                            </div>

                            <div className='inputFieldClild'>
                                <div
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'space-between',
                                    }}
                                >
                                    <label>Password</label>
                                    <p
                                        className='link'
                                        onClick={() =>
                                            history?.push('/forgot-password')
                                        }
                                    >
                                        Forgot your password?
                                    </p>
                                </div>
                                <input
                                    autoComplete='off'
                                    placeholder='Enter your password'
                                    type='password'
                                    value={data?.password}
                                    name='password'
                                    onChange={e => handelOnChange(e?.target)}
                                    onFocus={() => setError(false)}
                                    style={{
                                        borderColor: error ? '#FF333380' : null,
                                    }}
                                />
                                {error ? (
                                    <p className='loginError'>{error}</p>
                                ) : (
                                    void 0
                                )}
                            </div>
                        </div>

                        <div className='btnContainer'>
                            <div className='btn' onClick={onSubmit}>
                                <p>Login</p>
                            </div>
                        </div>

                        <div className='createNewAccount'>
                            <p>
                                New to MyJobs?{' '}
                                <span onClick={() => history?.push('/signup')}>
                                    Create an account
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default withRouter(SignIn);

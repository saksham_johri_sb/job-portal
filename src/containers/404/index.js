import React from 'react';
import { withRouter } from 'react-router';
import './style.scss';

const PageNotFound = props => {
    const { history = {} } = props || {};

    return (
        <div className='pageNotFound'>
            <div className='btn' onClick={() => history.push('/')}>
                <p>Home Page</p>
            </div>
        </div>
    );
};

export default withRouter(PageNotFound);

import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import Dashboard from '../../../components/dashboard';
import {
    getAllJobs,
    applyForJob,
} from '../../../redux/action/candidate/getAllJobs';
import loadingImage from '../../../assets/images/loading.gif';

const CandidateDashboard = props => {
    const {
        history = {},
        forceHideModal,
        setForceHideModal = () => {},
    } = props || {};

    const dispatch = useDispatch();

    const {
        allJobs: { isLoading, jobs },
    } = useSelector(state => state);

    const TITLE = 'Job Portal - Dashboard';

    const [jobList, setJobList] = useState([]);

    useEffect(() => {
        dispatch(getAllJobs());

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        setJobList(jobs);
    }, [jobs]);

    const onClick = id => {
        dispatch(applyForJob(id));
        setForceHideModal(!forceHideModal);
    };

    if (isLoading)
        return (
            <div className='loading'>
                <img src={loadingImage} alt='Loading...' height={40} />
            </div>
        );

    return (
        <>
            <Helmet>
                <title>{TITLE}</title>
            </Helmet>

            <Dashboard
                array={jobList}
                cardText='Apply'
                onClick={id => onClick(id)}
                homeText='Home'
                homeOnClick={() => history.push('/candidate/dashboard')}
                heading='Jobs for you'
                noCardText='No jobs available!'
            />
        </>
    );
};

export default withRouter(CandidateDashboard);

import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import Dashboard from '../../../components/dashboard';
import { getAppliedJobs } from '../../../redux/action/candidate/getAppliedJobs';
import loadingImage from '../../../assets/images/loading.gif';

const AppliedJobs = props => {
    const { history = {} } = props || {};

    const dispatch = useDispatch();

    const {
        appliedJobs: { isLoading, jobs },
    } = useSelector(state => state);

    const TITLE = 'Job Portal - Applied Jobs';

    const [jobList, setJobList] = useState([]);

    useEffect(() => {
        dispatch(getAppliedJobs());

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        setJobList(jobs);
    }, [jobs]);

    if (isLoading)
        return (
            <div className='loading'>
                <img src={loadingImage} alt='Loading...' height={40} />
            </div>
        );

    return (
        <>
            <Helmet>
                <title>{TITLE}</title>
            </Helmet>

            <Dashboard
                array={jobList}
                homeText='Home > Applied Jobs'
                homeOnClick={() => history.push('/candidate/dashboard')}
                heading='Jobs applied by you'
                noCardText='Your applied jobs will show here!'
                noCardBtnText='See all jobs'
                noCardOnClick={() => history.push('/candidate/dashboard')}
            />
        </>
    );
};

export default withRouter(AppliedJobs);

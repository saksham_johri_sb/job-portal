import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router';
import {
    changePassword,
    verifyPasswordToken,
} from '../../redux/action/forgotPasssword';
import { resetPasswordValidation } from '../../utils/validations';
import './style.scss';
import loadingImage from '../../assets/images/loading.gif';

const ResetPassword = props => {
    const TITLE = 'Job Portal - Reset Password';

    const { history = {} } = props || {};

    const dispatch = useDispatch();

    const { forgotPassword } = useSelector(state => state);
    const {
        token: resetPasswordToken,
        error: apiError,
        isPasswordChanged,
        isLoading,
        changePasswordError,
    } = forgotPassword || {};

    useEffect(() => {
        if (!resetPasswordToken) {
            history?.goBack();
            return;
        }

        dispatch(verifyPasswordToken(resetPasswordToken));

        if (apiError) {
            history?.goBack();
            return;
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (isPasswordChanged) {
            history.push('/login');
            return;
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [forgotPassword]);

    useEffect(() => {
        if (changePasswordError === 'Request failed with status code 422') {
            setError('Please enter a different password');
            return;
        }

        setError(changePasswordError);
    }, [changePasswordError]);

    const [data, setData] = useState({
        password: '',
        confirmPassword: '',
    });

    const handelOnChange = ({ name, value }) => {
        setData({
            ...data,
            [name]: value,
        });
    };

    const [error, setError] = useState(false);

    const onReset = () => {
        const checkValidations = resetPasswordValidation({
            ...data,
            setError,
        });
        if (checkValidations) {
            dispatch(changePassword({ ...data, token: resetPasswordToken }));
        }
    };

    if (isLoading)
        return (
            <div className='loading'>
                <img src={loadingImage} alt='Loading...' />
            </div>
        );

    return (
        <>
            <Helmet>
                <title>{TITLE}</title>
            </Helmet>

            <div className='reset'>
                <div className='reset-form-background' />

                <div className='resetContainer'>
                    <div className='resetInnerContainer come-to-front'>
                        <div className='resetTitle'>
                            <p>Reset Your Password</p>
                        </div>

                        <p className='resetDecs'>
                            Enter your new password below.
                        </p>

                        <div className='inputFields'>
                            <div className='inputFieldClild'>
                                <label>New password</label>
                                <input
                                    autoComplete='off'
                                    placeholder='Enter your password'
                                    type='password'
                                    value={data?.password}
                                    name='password'
                                    onChange={e => handelOnChange(e?.target)}
                                    onFocus={() => setError(false)}
                                    style={{
                                        borderColor: error ? '#FF333380' : null,
                                    }}
                                />
                            </div>

                            <div className='inputFieldClild'>
                                <label>Confirm new password</label>
                                <input
                                    autoComplete='off'
                                    placeholder='Enter your password'
                                    type='password'
                                    value={data?.confirmPassword}
                                    name='confirmPassword'
                                    onChange={e => handelOnChange(e?.target)}
                                    onFocus={() => setError(false)}
                                    style={{
                                        borderColor: error ? '#FF333380' : null,
                                    }}
                                />

                                {error ? (
                                    <p className='resetError'>{error}</p>
                                ) : (
                                    void 0
                                )}
                            </div>
                        </div>

                        <div className='btnContainer'>
                            <div className='btn' onClick={onReset}>
                                <p>Reset</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default withRouter(ResetPassword);

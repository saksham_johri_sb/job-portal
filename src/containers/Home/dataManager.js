import solayticIcon from '../../assets/images/solayticIcon.png';
import amaraIcon from '../../assets/images/amaraIcon.png';
import avenIcon from '../../assets/images/avenIcon.png';
import foxhubIcon from '../../assets/images/foxhubIcon.png';
import hexLabIcon from '../../assets/images/hexLabIcon.png';
import lightIcon from '../../assets/images/lightIcon.png';
import trevaIcon from '../../assets/images/trevaIcon.png';
import velocityIcon from '../../assets/images/velocityIcon.png';
import zootvIcon from '../../assets/images/zootvIcon.png';

export const cardList = [
    {
        title: 'Get more visibility',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    },
    {
        title: 'Organize your candidates',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    },
    {
        title: 'Verify their abilities',
        description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.',
    },
];

export const companiesLogo = [
    { alt: 'Solaytic', icon: solayticIcon },
    { alt: 'Amara', icon: amaraIcon },
    { alt: 'Aven', icon: avenIcon },
    { alt: 'Foxhub', icon: foxhubIcon },
    { alt: 'HexLab', icon: hexLabIcon },
    { alt: 'Light', icon: lightIcon },
    { alt: 'Treva', icon: trevaIcon },
    { alt: 'Velocity', icon: velocityIcon },
    { alt: 'Zootv', icon: zootvIcon },
];

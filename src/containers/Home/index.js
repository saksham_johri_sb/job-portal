import React from 'react';
import './style.scss';
import { withRouter } from 'react-router';
import { Helmet } from 'react-helmet';
import { cardList, companiesLogo } from './dataManager';
const homeImage =
    'https://images.pexels.com/photos/3756679/pexels-photo-3756679.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260';

const Home = props => {
    const { history = {} } = props || {};

    const TITLE = 'Job Portal - Home';

    const renderCard = ({ item, index }) => {
        const { title, description } = item || {};
        return (
            <div key={index?.toString()}>
                <h3>{title}</h3>
                <p>{description}</p>
            </div>
        );
    };

    return (
        <>
            <Helmet>
                <title>{TITLE}</title>
            </Helmet>

            <div className='homeContainer'>
                <div>
                    <div className='homeHeading'>
                        <p>Welcome to </p>
                        <p className='title'>
                            My<span>Jobs</span>
                        </p>
                    </div>

                    <div
                        className='btn'
                        onClick={() => history?.push('/signup')}
                    >
                        <p>Get Started</p>
                    </div>
                </div>

                <img src={homeImage} className='homeImage' alt='homeImage' />
            </div>

            {cardList?.length ? (
                <div className='whyUsContainer'>
                    <div className='whyUs'>
                        <p>Why Us</p>

                        <div className='whyUsCardContainer'>
                            {cardList?.map((item, index) =>
                                renderCard({ item, index })
                            )}
                        </div>
                    </div>
                </div>
            ) : (
                void 0
            )}

            {companiesLogo?.length ? (
                <div className='companies'>
                    <p>companies who trust us</p>
                    <div>
                        {companiesLogo?.map((item, index) => {
                            const { alt = 'Logo', icon } = item || {};

                            return (
                                <img
                                    key={index?.toString()}
                                    src={icon}
                                    alt={alt}
                                    width={100}
                                    height={40}
                                />
                            );
                        })}
                    </div>
                </div>
            ) : (
                void 0
            )}
        </>
    );
};

export default withRouter(Home);

import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router';
import './style.scss';
import { Helmet } from 'react-helmet';
import { signupValidation } from '../../utils/validations';
import recruiterActiveIcon from '../../assets/images/recruiterActiveIcon.png';
import recruiterIcon from '../../assets/images/recruiterIcon.png';
import candidateIcon from '../../assets/images/candidateIcon.png';
import candidateActiveIcon from '../../assets/images/candidateActiveIcon.png';
import { useDispatch, useSelector } from 'react-redux';
import { signUp } from '../../redux/action/auth';
import loadingImage from '../../assets/images/loading.gif';

const SignUp = props => {
    const { history = {} } = props || {};

    const dispatch = useDispatch();
    const { authentication } = useSelector(state => state);
    const {
        isLoading,
        userData: { userRole } = {},
        signUpError,
    } = authentication || {};

    useEffect(() => {
        if (signUpError) {
            if (signUpError === 'Request failed with status code 422') {
                setError('Account already exist');
                return;
            }

            setError(signUpError);
            return;
        }

        if (userRole === 0 && localStorage?.getItem('userRole')) {
            history.push('/recruiter/dashboard');
        } else if (userRole === 1 && localStorage?.getItem('userRole')) {
            history.push('/candidate/dashboard');
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [authentication]);

    const TITLE = 'Job Portal - SignUp';

    const [data, setData] = useState({
        userRole: 0,
        name: '',
        email: '',
        password: '',
        confirmPassword: '',
        skills: '',
    });

    const handelOnChange = ({ name, value }) => {
        setData({
            ...data,
            [name]: value,
        });
    };

    const [nameError, setNameError] = useState(false);
    const [emailError, setEmailError] = useState(false);
    const [passwordError, setPasswordError] = useState(false);
    const [skillsError, setSkillsError] = useState(false);
    const [error, setError] = useState(false);

    useEffect(() => {
        setData({
            ...data,
            name: '',
            email: '',
            password: '',
            confirmPassword: '',
            skills: '',
        });

        setNameError(false);
        setEmailError(false);
        setPasswordError(false);
        setSkillsError(false);
        setError(false);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [data.userRole]);

    const onSignUp = async () => {
        setError(false);

        const checkValidations = signupValidation({
            ...data,
            setNameError,
            setEmailError,
            setPasswordError,
            setSkillsError,
        });

        if (checkValidations) {
            dispatch(signUp({ ...data }));
        }
    };

    if (isLoading)
        return (
            <div className='loading'>
                <img src={loadingImage} alt='Loading...' height={40} />
            </div>
        );

    return (
        <>
            <Helmet>
                <title>{TITLE}</title>
            </Helmet>

            <div className='signUp'>
                <div className='signup-form-background' />

                <div className='signUpContainer'>
                    <div className='signUpInnerContainer come-to-front'>
                        <div className='signUpTitle'>
                            <p>
                                <b>Signup</b>
                            </p>
                        </div>

                        <div className='inputFields'>
                            <p className='who'>
                                I'm a<span className='required'>*</span>
                            </p>

                            <div className='btnSelection'>
                                <div
                                    className='btn'
                                    style={{
                                        background:
                                            data?.userRole === 0
                                                ? void 0
                                                : '#E8E8E833 0% 0% no-repeat padding-box',
                                        border:
                                            data?.userRole === 0
                                                ? void 0
                                                : '1px solid #C6C6C6',
                                        color:
                                            data?.userRole === 0
                                                ? void 0
                                                : '#303F60',
                                    }}
                                    onClick={() =>
                                        handelOnChange({
                                            name: 'userRole',
                                            value: 0,
                                        })
                                    }
                                >
                                    <img
                                        src={
                                            data?.userRole === 0
                                                ? recruiterActiveIcon
                                                : recruiterIcon
                                        }
                                        alt='recruiter'
                                    />
                                    <p
                                        style={{
                                            color:
                                                data?.userRole === 0
                                                    ? '#fff'
                                                    : '#303F60',
                                        }}
                                    >
                                        Recruiter
                                    </p>
                                </div>

                                <div
                                    className='btn'
                                    style={{
                                        background:
                                            data?.userRole === 1
                                                ? void 0
                                                : '#E8E8E833 0% 0% no-repeat padding-box',
                                        border:
                                            data?.userRole === 1
                                                ? void 0
                                                : '1px solid #C6C6C6',
                                        color:
                                            data?.userRole === 1
                                                ? void 0
                                                : '#303F60',
                                    }}
                                    onClick={() =>
                                        handelOnChange({
                                            name: 'userRole',
                                            value: 1,
                                        })
                                    }
                                >
                                    <img
                                        src={
                                            data?.userRole === 1
                                                ? candidateActiveIcon
                                                : candidateIcon
                                        }
                                        alt='candidate'
                                    />
                                    <p
                                        style={{
                                            color:
                                                data?.userRole === 1
                                                    ? '#fff'
                                                    : '#303F60',
                                        }}
                                    >
                                        Candidate
                                    </p>
                                </div>
                            </div>

                            <div className='inputFieldClild'>
                                <label>
                                    Full Name<span className='required'>*</span>
                                </label>
                                <input
                                    autoComplete='off'
                                    placeholder='Enter your full name'
                                    type='text'
                                    name='name'
                                    value={data?.name}
                                    onChange={e => handelOnChange(e?.target)}
                                    onFocus={() => setNameError(false)}
                                    style={{
                                        borderColor: nameError
                                            ? '#FF333380'
                                            : null,
                                    }}
                                />
                                {nameError ? (
                                    <p className='signUpError'>{nameError}</p>
                                ) : (
                                    void 0
                                )}
                            </div>

                            <div className='inputFieldClild'>
                                <label>
                                    Email Address
                                    <span className='required'>*</span>
                                </label>
                                <input
                                    autoComplete='off'
                                    placeholder='Enter your email'
                                    type='email'
                                    value={data?.email}
                                    name='email'
                                    onChange={e => handelOnChange(e?.target)}
                                    onFocus={() => setEmailError(false)}
                                    style={{
                                        borderColor: emailError
                                            ? '#FF333380'
                                            : null,
                                    }}
                                />

                                {emailError ? (
                                    <p className='signUpError'>{emailError}</p>
                                ) : (
                                    void 0
                                )}
                            </div>

                            <div className='inputFieldBox'>
                                <div className='inputFieldClild'>
                                    <label>
                                        Create Password
                                        <span className='required'>*</span>
                                    </label>
                                    <input
                                        autoComplete='off'
                                        placeholder='Enter your password'
                                        type='password'
                                        value={data?.password}
                                        name='password'
                                        onChange={e =>
                                            handelOnChange(e?.target)
                                        }
                                        onFocus={() => setPasswordError(false)}
                                        style={{
                                            borderColor: passwordError
                                                ? '#FF333380'
                                                : null,
                                        }}
                                    />
                                </div>

                                <div className='inputFieldClild'>
                                    <label>
                                        Confirm Password
                                        <span className='required'>*</span>
                                    </label>
                                    <input
                                        autoComplete='off'
                                        placeholder='Enter your password'
                                        type='password'
                                        value={data?.confirmPassword}
                                        name='confirmPassword'
                                        onChange={e =>
                                            handelOnChange(e?.target)
                                        }
                                        onFocus={() => setPasswordError(false)}
                                        style={{
                                            borderColor: passwordError
                                                ? '#FF333380'
                                                : null,
                                        }}
                                    />

                                    {passwordError ? (
                                        <p className='signUpError'>
                                            {passwordError}
                                        </p>
                                    ) : (
                                        void 0
                                    )}
                                </div>
                            </div>

                            <div className='inputFieldClild'>
                                <label>
                                    Skills
                                    {data?.userRole === 1 && (
                                        <span className='required'>*</span>
                                    )}
                                </label>
                                <input
                                    autoComplete='off'
                                    placeholder='Enter comma separated skills'
                                    type='text'
                                    value={data?.skills}
                                    name='skills'
                                    onChange={e => handelOnChange(e?.target)}
                                    onFocus={() => setSkillsError(false)}
                                    style={{
                                        borderColor: skillsError
                                            ? '#FF333380'
                                            : null,
                                    }}
                                />

                                {skillsError ? (
                                    <p className='signUpError'>{skillsError}</p>
                                ) : (
                                    void 0
                                )}
                            </div>
                        </div>

                        {error ? (
                            <p className='somethingWentWrong'>{error}</p>
                        ) : (
                            void 0
                        )}

                        <div className='btnContainer'>
                            <div className='btn' onClick={onSignUp}>
                                <p>Signup</p>
                            </div>
                        </div>

                        <div className='haveAccount'>
                            <p>
                                Have an account?{' '}
                                <span onClick={() => history?.push('/login')}>
                                    Login
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default withRouter(SignUp);

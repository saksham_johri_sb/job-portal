import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router';
import './style.scss';
import downArrowIcon from '../../assets/images/downArrowIcon.png';
import closeIcon from '../../assets/images/closeIcon.png';

const Header = props => {
    const { history = {}, location = {}, forceHideModal } = props || {};

    const [pathName, setPathName] = useState(location?.pathname);

    const [showNavToLogin, setShowNavToLogin] = useState(false);
    const [showUserSection, setShowUserSection] = useState(false);

    const [userName, setUserName] = useState(localStorage?.getItem('name'));

    const [showLogout, setShowLogout] = useState(false);
    const [showSuccessfullLogout, setShowSuccessfullLogout] = useState(false);

    const [headerLink, setHeaderLink] = useState(void 0);

    const recruiter = (
        <p
            className='jobText'
            onClick={() => history?.push('/recruiter/post-job')}
            style={{
                opacity: pathName === '/recruiter/post-job' ? 1 : 0.8,
            }}
        >
            Post a Job
        </p>
    );

    const candidate = (
        <p
            className='jobText'
            onClick={() => history?.push('/candidate/applied-jobs')}
            style={{
                opacity: pathName === '/candidate/applied-jobs' ? 1 : 0.8,
            }}
        >
            Applied Jobs
        </p>
    );

    useEffect(() => {
        setPathName(location?.pathname);

        setShowLogout(false);
    }, [location?.pathname]);

    useEffect(() => {
        setUserName(localStorage?.getItem('name'));

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [localStorage.getItem('name')]);

    useEffect(() => {
        setShowNavToLogin(false);
        setShowUserSection(false);
        setHeaderLink(void 0);

        switch (pathName) {
            case '/':
                setShowNavToLogin(true);
                break;

            case '/forgot-password':
                setShowNavToLogin(true);
                break;

            case '/reset-password':
                setShowNavToLogin(true);
                break;

            case '/recruiter/dashboard':
                setShowUserSection(true);
                setHeaderLink(recruiter);
                break;

            case '/recruiter/post-job':
                setShowUserSection(true);
                setHeaderLink(recruiter);
                break;

            case '/candidate/dashboard':
                setShowUserSection(true);
                setHeaderLink(candidate);
                break;

            case '/candidate/applied-jobs':
                setShowUserSection(true);
                setHeaderLink(candidate);
                break;

            default:
                setShowNavToLogin(false);
                setShowUserSection(false);
                break;
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pathName]);

    useEffect(() => {
        setShowLogout(false);
    }, [forceHideModal]);

    const logout = () => {
        setShowLogout(false);
        localStorage?.clear();
        history?.push('/');

        setShowSuccessfullLogout(true);
        setTimeout(() => {
            setShowSuccessfullLogout(false);
        }, 3000);
    };

    return (
        <div className='header-container'>
            <div className='header'>
                <div
                    className='title navTitle'
                    onClick={() => history?.push('/')}
                >
                    <p>
                        My<span>Jobs</span>
                    </p>
                </div>

                {showNavToLogin ? (
                    <div
                        className='navBtn'
                        onClick={() => history?.push('/login')}
                    >
                        <p>Login/Signup</p>
                    </div>
                ) : (
                    void 0
                )}

                {showUserSection ? (
                    <div className='jobSection'>
                        {headerLink}

                        <div className='user'>
                            <div className='userProfile'>
                                <p>
                                    {userName
                                        ? userName[0]?.toUpperCase()
                                        : '/'}
                                </p>
                            </div>
                            <img
                                src={downArrowIcon}
                                onClick={() => setShowLogout(!showLogout)}
                                alt=''
                            />
                        </div>

                        {showLogout ? (
                            <div className='logoutTile' onClick={logout}>
                                <p>Logout</p>
                            </div>
                        ) : (
                            void 0
                        )}
                    </div>
                ) : (
                    void 0
                )}

                {showSuccessfullLogout ? (
                    <div className='successfullLogout'>
                        <img
                            src={closeIcon}
                            onClick={() => setShowSuccessfullLogout(false)}
                            alt='close'
                            height={15}
                        />
                        <div>
                            <p className='first'>Logout</p>
                            <p className='second'>
                                You have successfully logged out.
                            </p>
                        </div>
                    </div>
                ) : (
                    void 0
                )}
            </div>
        </div>
    );
};

export default withRouter(Header);

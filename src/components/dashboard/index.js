import React, { useState, useEffect } from 'react';
import './style.scss';
import homeIcon from '../../assets/images/homeIcon.png';
import writtingIcon from '../../assets/images/writtingIcon.png';
import JobCard from '../jobCard';
import nextIcon from '../../assets/images/nextIcon.png';
import previousIcon from '../../assets/images/previousIcon.png';

const Dashboard = props => {
    const {
        array = [],
        cardText = '',
        onClick = () => {},
        homeText,
        homeOnClick = () => {},
        heading,
        noCardText,
        noCardBtnText,
        noCardOnClick = () => {},
    } = props || {};

    const NUMBER_OF_CARDS_PER_PAGE = 16;

    const [visibleArray, setVisibleArray] = useState([]);

    const [initialPos, setInitialPos] = useState(0);
    const [finalPos, setFinalPos] = useState(NUMBER_OF_CARDS_PER_PAGE);

    const sliceJobCard = (task = '') => {
        let jobs = [];

        if (task === 'next') {
            jobs = array?.slice(
                initialPos + NUMBER_OF_CARDS_PER_PAGE,
                finalPos + NUMBER_OF_CARDS_PER_PAGE
            );

            setInitialPos(initialPos + NUMBER_OF_CARDS_PER_PAGE);
            setFinalPos(finalPos + NUMBER_OF_CARDS_PER_PAGE);
        } else if (task === 'prev') {
            jobs = array?.slice(
                initialPos - NUMBER_OF_CARDS_PER_PAGE,
                finalPos - NUMBER_OF_CARDS_PER_PAGE
            );

            setInitialPos(initialPos - NUMBER_OF_CARDS_PER_PAGE);
            setFinalPos(finalPos - NUMBER_OF_CARDS_PER_PAGE);
        } else {
            jobs = array?.slice(initialPos, finalPos);
        }

        setVisibleArray(jobs);
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => sliceJobCard(), [array, initialPos, finalPos]);

    const Pagination = props => {
        const numberOfPages = Math.ceil(
            array?.length / NUMBER_OF_CARDS_PER_PAGE
        );

        const currentPageNumber = initialPos / NUMBER_OF_CARDS_PER_PAGE + 1;

        let i = 1;
        const range = [];

        while (i <= numberOfPages) {
            range.push(i);

            i += 1;
        }

        return (
            <>
                {range?.map((item, index) => {
                    return (
                        <div
                            key={index}
                            className='currentPage'
                            style={{
                                backgroundColor:
                                    currentPageNumber !== item
                                        ? 'transparent'
                                        : void 0,
                            }}
                            onClick={() => {
                                setInitialPos(
                                    NUMBER_OF_CARDS_PER_PAGE * (item - 1)
                                );
                                setFinalPos(
                                    NUMBER_OF_CARDS_PER_PAGE * (item - 1) +
                                        NUMBER_OF_CARDS_PER_PAGE
                                );
                            }}
                        >
                            <p>{item}</p>
                        </div>
                    );
                })}
            </>
        );
    };

    return (
        <>
            <div className='dashboard'>
                <div
                    className='dashboard-form-background'
                    style={{ height: array?.length ? '11rem' : void 0 }}
                />

                <div className='dashboardContainer come-to-front'>
                    <div className='dashboardInnerContainer'>
                        <div className='homeIcon' onClick={homeOnClick}>
                            <img src={homeIcon} alt='home' height={13} />
                            <p>{homeText}</p>
                        </div>

                        <h2>{heading}</h2>

                        {array?.length ? (
                            <>
                                <div className='jobContainer'>
                                    {visibleArray?.map((item, index) => (
                                        <JobCard
                                            key={index}
                                            item={item}
                                            index={index}
                                            cardText={cardText}
                                            onClick={onClick}
                                        />
                                    ))}
                                </div>

                                <div className='jobCardNav'>
                                    <img
                                        src={previousIcon}
                                        alt='previous'
                                        style={{
                                            cursor:
                                                initialPos === 0
                                                    ? 'not-allowed'
                                                    : 'pointer',
                                        }}
                                        onClick={
                                            initialPos === 0
                                                ? () => {}
                                                : () => sliceJobCard('prev')
                                        }
                                    />

                                    <Pagination />

                                    <img
                                        src={nextIcon}
                                        alt='next'
                                        style={{
                                            cursor:
                                                finalPos >= array?.length
                                                    ? 'not-allowed'
                                                    : 'pointer',
                                        }}
                                        onClick={
                                            finalPos >= array?.length
                                                ? () => {}
                                                : () => sliceJobCard('next')
                                        }
                                    />
                                </div>
                            </>
                        ) : (
                            <div className='noJobContainer'>
                                <div className='noJob'>
                                    <img src={writtingIcon} alt='no-post' />
                                    <p className='text'>{noCardText}</p>
                                    {noCardBtnText ? (
                                        <div
                                            className='btn'
                                            onClick={noCardOnClick}
                                        >
                                            <p>{noCardBtnText}</p>
                                        </div>
                                    ) : (
                                        void 0
                                    )}
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </>
    );
};

export default Dashboard;

import React from 'react';
import './style.scss';
import mapMarker from '../../assets/images/mapMarker.png';

const JobCard = props => {
    const { item, index, cardText, onClick = () => {} } = props || {};

    const { title, description, location, id } = item || {};

    return (
        <div key={index?.toString()} className='jobCard'>
            <div className='jobCardUpperConatiner'>
                <p className='jobTitle'>{title}</p>

                <p className='jobDescription'>{description}</p>
            </div>

            <div className='jobCardBottomContainer'>
                <div className='location-tootip-container'>
                    <div
                        className='location'
                        style={{ maxWidth: !cardText ? '12rem' : void 0 }}
                    >
                        <img src={mapMarker} alt='location' height={15} />
                        <p className='locationName'>{location}</p>
                    </div>

                    <span className='location-tooltip'>{location}</span>
                </div>

                {cardText ? (
                    <div
                        className='viewApplication'
                        onClick={() => onClick(id)}
                    >
                        <p>{cardText}</p>
                    </div>
                ) : (
                    void 0
                )}
            </div>
        </div>
    );
};

export default JobCard;

import { useState } from 'react';
import { BrowserRouter } from 'react-router-dom';
import './App.scss';
import Header from './components/header';
import Routes from './routes';

const App = () => {
    const [forceHideModal, setForceHideModal] = useState(false);

    return (
        <div className='App'>
            <BrowserRouter>
                <Header
                    forceHideModal={forceHideModal}
                    setForceHideModal={setForceHideModal}
                />

                <Routes
                    forceHideModal={forceHideModal}
                    setForceHideModal={setForceHideModal}
                />
            </BrowserRouter>
        </div>
    );
};

export default App;

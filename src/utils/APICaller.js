import axios from 'axios';

const APICaller = ({ method, url, data }) => {
    const option = {
        method,
        url: process.env.REACT_APP_BASE_URL + url,
        data,
        headers: {
            'content-type': 'application/json',
            Accept: '*/*',
            Authorization: localStorage.getItem('token') || null,
        },
    };

    return new Promise((resolve, reject) => {
        axios({ ...option })
            .then(res => {
                resolve(res.data);
            })
            .catch(err => {
                reject(err);
            });
    });
};

export default APICaller;

const validateEmail = email => {
    const mailformat = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;

    if (email?.match(mailformat)) return true;

    return false;
};

const onlyNumericRegix = text => {
    const regex = /^[0-9]*$/;

    if (text?.match(regex)) return true;

    return false;
};

export const signupValidation = ({
    name,
    email,
    password,
    confirmPassword,
    userRole,
    skills,

    setNameError = () => {},
    setEmailError = () => {},
    setPasswordError = () => {},
    setSkillsError = () => {},
}) => {
    let result = true;

    let nameRegex = /^[A-Za-z]+$/;

    if (!name?.length) {
        setNameError('The field is mandatory.');
        result = false;
    } else if (name?.length < 3) {
        setNameError('Length shoud be greater than 2 characters');
        result = false;
    } else if (!name?.match(nameRegex)) {
        setNameError('Name cannot have special characters and numbers.');
        result = false;
    }

    if (!email?.length) {
        setEmailError('The field is mandatory.');
        result = false;
    } else if (!validateEmail(email)) {
        setEmailError('Invalid email address.');
        result = false;
    }

    if (!password?.length || !confirmPassword?.length) {
        setPasswordError('The field is mandatory.');
        result = false;
    } else if (password?.length < 6 || confirmPassword?.length < 6) {
        setPasswordError('Password should be greater than 5 characters.');
        result = false;
    } else if (password !== confirmPassword) {
        setPasswordError('Password does not match.');
        result = false;
    }

    const trimmedSkills = skills?.trim();
    if (userRole === 1 && !trimmedSkills) {
        setSkillsError('This field is mandatory');
        result = false;
    }

    if (skills) {
        if (onlyNumericRegix(skills)) {
            setSkillsError('Only numbers are not allowed');
            result = false;
        }
    }

    return result;
};

export const loginValidation = ({ email, password, setError = () => {} }) => {
    let result = true;

    if (!email?.length || !password?.length) {
        setError('All fields are mandatory.');
        result = false;
    } else if (!validateEmail(email)) {
        setError('Enter valid email.');
        result = false;
    } else if (password?.length < 6) {
        setError('Password should be greater than 5 characters.');
        result = false;
    }

    return result;
};

export const forgotPasswordValidation = ({ email, setError = () => {} }) => {
    if (email?.length) {
        if (!validateEmail(email)) {
            setError('Invalid email address.');
            return false;
        }

        return true;
    }
    setError('The field is mandatory.');
    return false;
};

export const resetPasswordValidation = ({
    password,
    confirmPassword,
    setError = () => {},
}) => {
    if (password?.length && confirmPassword?.length) {
        if (password !== confirmPassword) {
            setError('Password does not match.');
            return false;
        }

        return true;
    }
    setError('The field(s) is/are mandatory.');
    return false;
};

export const postJobValidation = ({
    title,
    description,
    location,

    setTitleError = () => {},
    setDescriptionError = () => {},
    setLocationError = () => {},
}) => {
    let result = true;

    if (!title?.trim()?.length) {
        setTitleError('This field is mandatory');
        result = false;
    } else if (!(title?.trim()?.length > 2 && title?.trim()?.length < 100)) {
        setTitleError('Length should be between 3 to 100 characters');
        result = false;
    } else if (onlyNumericRegix(title)) {
        setTitleError('Only numbers are not allowed');
        result = false;
    }

    if (!description?.trim()?.length) {
        setDescriptionError('This field is mandatory');
        result = false;
    } else if (
        !(description?.trim()?.length > 2 && description?.trim()?.length < 150)
    ) {
        setDescriptionError('Length should be between 3 to 100 characters');
        result = false;
    }

    if (!location?.trim()?.length) {
        setLocationError('This field is mandatory');
        result = false;
    } else if (
        !(location?.trim()?.length > 2 && location?.trim()?.length < 100)
    ) {
        setLocationError('Length should be between 3 to 100 characters');
        result = false;
    }

    return result;
};

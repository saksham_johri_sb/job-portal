import React from 'react';
import { Route, Switch } from 'react-router-dom';
import ForgotPassword from '../containers/ForgotPassword';
import Home from '../containers/Home';
import ResetPassword from '../containers/ResetPassword';
import SignIn from '../containers/SignIn';
import SignUp from '../containers/Signup';
import RecruiterDashboard from '../containers/recruiter/Dashboard';
import PostJob from '../containers/recruiter/PostJob';
import CandidateDashboard from '../containers/candidate/Dashboard';
import AppliedJob from '../containers/candidate/AppliedJob';
import { ProtectedRoute, PublicRoute } from './customRoute';
import PageNotFound from '../containers/404';

const Routes = props => {
    return (
        <Switch>
            <PublicRoute exact path='/' component={Home} />
            <PublicRoute exact path='/signup' component={SignUp} />
            <PublicRoute exact path='/login' component={SignIn} />
            <PublicRoute
                exact
                path='/forgot-password'
                component={ForgotPassword}
            />
            <PublicRoute
                exact
                path='/reset-password'
                component={ResetPassword}
            />

            <ProtectedRoute
                exact
                path='/recruiter/dashboard'
                component={RecruiterDashboard}
                {...props}
            />
            <ProtectedRoute
                exact
                path='/recruiter/post-job'
                component={PostJob}
            />
            <ProtectedRoute
                exact
                path='/candidate/dashboard'
                component={CandidateDashboard}
                {...props}
            />
            <ProtectedRoute
                exact
                path='/candidate/applied-jobs'
                component={AppliedJob}
            />

            <Route path='*' component={PageNotFound} />
        </Switch>
    );
};

export default Routes;

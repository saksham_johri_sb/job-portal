/* eslint-disable eqeqeq */
import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import Home from '../containers/Home';
import CandidateDashboard from '../containers/candidate/Dashboard';
import RecruiterDashboard from '../containers/recruiter/Dashboard';

export const ProtectedRoute = ({
    component: Component,
    forceHideModal,
    setForceHideModal,
    ...rest
}) => {
    const { path } = rest || {};

    if (!localStorage?.getItem('token')) {
        return <Redirect to='/' component={Home} />;
    }

    if (localStorage?.getItem('userRole') == 0 && path?.includes('candidate'))
        return (
            <Redirect
                to='/recruiter/dashboard'
                component={RecruiterDashboard}
            />
        );

    if (localStorage?.getItem('userRole') == 1 && path?.includes('recruiter'))
        return (
            <Redirect
                to='/candidate/dashboard'
                component={RecruiterDashboard}
            />
        );

    return (
        <Route {...rest}>
            <Component
                forceHideModal={forceHideModal}
                setForceHideModal={setForceHideModal}
            />
        </Route>
    );
};

export const PublicRoute = ({ component: Component, ...rest }) => {
    if (localStorage?.getItem('token')) {
        if (localStorage?.getItem('userRole') == 0) {
            return (
                <Redirect
                    to='/recruiter/dashboard'
                    component={RecruiterDashboard}
                />
            );
        }

        if (localStorage?.getItem('userRole') == 1)
            return (
                <Redirect
                    to='/candidate/dashboard'
                    component={CandidateDashboard}
                />
            );
    }

    return <Route {...rest} component={Component} />;
};

import { all } from 'redux-saga/effects';
import authSaga from './auth';
import getAllJobsSaga from './candidate/getAllJobs';
import getAppliedJobsSaga from './candidate/getAppliedJobs';
import forgotPasswordSaga from './forgotPassword';
import getJobApplicantsSaga from './recruiter/getJobApplicants';
import postedJobsSaga from './recruiter/getPostedJobs';
import postJobSaga from './recruiter/postJob';

export default function* rootSaga() {
    yield all([
        authSaga(),
        forgotPasswordSaga(),
        postedJobsSaga(),
        getJobApplicantsSaga(),
        postJobSaga(),
        getAllJobsSaga(),
        getAppliedJobsSaga(),
    ]);
}

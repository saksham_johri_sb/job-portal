import { all, call, put, takeLeading } from '@redux-saga/core/effects';
import { createJob } from '../../api';
import {
    postJobFailed,
    postJobSuccess,
} from '../../redux/action/recruiter/postJob';
import { POST_JOB } from '../../redux/types';

function* postJob({ payload }) {
    try {
        const data = yield call(createJob, payload);
        yield put(postJobSuccess(data));
    } catch (error) {
        yield put(postJobFailed(error.message));
    }
}

function* postJobWatcher() {
    yield takeLeading(POST_JOB, postJob);
}

export default function* postJobSaga() {
    yield all([postJobWatcher()]);
}

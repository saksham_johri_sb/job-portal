import { all, call, put, takeLeading } from '@redux-saga/core/effects';
import { getPostedJobs } from '../../api';
import {
    getPostedJobsFailed,
    getPostedJobsSuccess,
} from '../../redux/action/recruiter/getPostedJobs';
import { GET_POSTED_JOBS } from '../../redux/types';

function* postedJobs({ payload }) {
    try {
        const data = yield call(getPostedJobs);
        yield put(getPostedJobsSuccess(data));
    } catch (error) {
        console.log('<<< Error in getting posted jobs array >>>', error);
        yield put(getPostedJobsFailed(error.message));
    }
}

function* getPostedJobsWatcher() {
    yield takeLeading(GET_POSTED_JOBS, postedJobs);
}

export default function* postedJobsSaga() {
    yield all([getPostedJobsWatcher()]);
}

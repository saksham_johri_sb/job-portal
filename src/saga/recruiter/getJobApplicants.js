import { all, call, put, takeLeading } from '@redux-saga/core/effects';
import { getApplicants } from '../../api';
import {
    getJobApplicantsFailed,
    getJobApplicantsSuccess,
} from '../../redux/action/recruiter/getJobApplicants';
import { GET_JOB_APPLICANTS } from '../../redux/types';

function* getJobApplicants({ payload }) {
    try {
        const data = yield call(getApplicants, payload);
        yield put(getJobApplicantsSuccess(data));
    } catch (error) {
        console.log('<<< Error in getting applicant list >>>', error);
        yield put(getJobApplicantsFailed(error.message));
    }
}

function* getJobApplicantsWatcher() {
    yield takeLeading(GET_JOB_APPLICANTS, getJobApplicants);
}

export default function* getJobApplicantsSaga() {
    yield all([getJobApplicantsWatcher()]);
}

import { all, call, takeLeading, put } from 'redux-saga/effects';
import { SIGN_IN, SIGN_UP } from '../redux/types';
import { login, signup } from '../api';
import {
    signInFailed,
    signInSuccess,
    signUpFailed,
    signUpSuccess,
} from '../redux/action/auth';

function* signInSaga({ payload }) {
    try {
        const data = yield call(login, payload);
        yield put(signInSuccess(data));
    } catch (error) {
        yield put(signInFailed(error.message));
    }
}

function* signUpSaga({ payload }) {
    try {
        const data = yield call(signup, payload);
        yield put(signUpSuccess(data));
    } catch (error) {
        yield put(signUpFailed(error.message));
    }
}

function* signInWatcher() {
    yield takeLeading(SIGN_IN, signInSaga);
}

function* signUpWatcher() {
    yield takeLeading(SIGN_UP, signUpSaga);
}

export default function* authenticationSaga() {
    yield all([signInWatcher(), signUpWatcher()]);
}

import { all, call, takeLeading, put } from 'redux-saga/effects';
import {
    getResetPasswordToken,
    verifyPasswordToken,
    changePassword as resetPassword,
} from '../api';
import {
    changePasswordFailed,
    changePasswordSuccess,
    getPasswordTokenFailed,
    savePasswordToken,
    verifyPasswordTokenFailed,
    verifyPasswordTokenSuccess,
} from '../redux/action/forgotPasssword';
import {
    CHANGE_PASSWORD,
    FORGOT_PASSWORD,
    VERIFY_PASSWORD_TOKEN,
} from '../redux/types';

function* getPasswordToken({ payload }) {
    try {
        const data = yield call(getResetPasswordToken, payload);
        yield put(savePasswordToken(data));
    } catch (error) {
        yield put(getPasswordTokenFailed(error.message));
    }
}

function* verifyToken({ payload }) {
    try {
        const data = yield call(verifyPasswordToken, payload);
        yield put(verifyPasswordTokenSuccess(data));
    } catch (error) {
        yield put(verifyPasswordTokenFailed(error.message));
    }
}

function* changePassword({ payload }) {
    try {
        const data = yield call(resetPassword, payload);
        yield put(changePasswordSuccess(data));
    } catch (error) {
        yield put(changePasswordFailed(error.message));
    }
}

function* forgotPasswordWatcher() {
    yield takeLeading(FORGOT_PASSWORD, getPasswordToken);
}

function* verifyTokenWatcher() {
    yield takeLeading(VERIFY_PASSWORD_TOKEN, verifyToken);
}

function* changePasswordWatcher() {
    yield takeLeading(CHANGE_PASSWORD, changePassword);
}

export default function* forgotPasswordSaga() {
    yield all([
        forgotPasswordWatcher(),
        verifyTokenWatcher(),
        changePasswordWatcher(),
    ]);
}

import { all, call, put, takeLeading } from 'redux-saga/effects';
import { GET_APPLIED_JOBS } from '../../redux/types';
import {
    getAppliedJobsFailed,
    getAppliedJobsSuccess,
} from '../../redux/action/candidate/getAppliedJobs';
import { appliedJobs } from '../../api';

function* getAppliedJobs({ payload }) {
    try {
        const data = yield call(appliedJobs);

        yield put(getAppliedJobsSuccess(data));
    } catch (error) {
        yield put(getAppliedJobsFailed(error.message));
    }
}

function* getAppliedJobsWatcher() {
    yield takeLeading(GET_APPLIED_JOBS, getAppliedJobs);
}

export default function* getAppliedJobsSaga() {
    yield all([getAppliedJobsWatcher()]);
}

import { all, call, put, takeLeading } from '@redux-saga/core/effects';
import { APPLY_FOR_JOB, GET_ALL_JOBS } from '../../redux/types';
import {
    getAllJobs,
    getAllJobsSuccess,
    getAllJobsFailed,
} from '../../redux/action/candidate/getAllJobs';
import { applyForJob, getJobs } from '../../api';

function* getAllJobsForCandidate({ payload }) {
    try {
        const data = yield call(getJobs);
        yield put(getAllJobsSuccess(data));
    } catch (error) {
        console.log('<<< Error in getting all jobs array >>>', error);
        yield put(getAllJobsFailed(error.message));
    }
}

function* applyJob({ payload }) {
    try {
        yield call(applyForJob, payload);
        yield put(getAllJobs());
    } catch (error) {
        console.log('<<< Error in getting all jobs array >>>', error);
        yield put(getAllJobsFailed(error.message));
    }
}

function* getAllJobsWatcher() {
    yield takeLeading(GET_ALL_JOBS, getAllJobsForCandidate);
}

function* applyForJobWatcher() {
    yield takeLeading(APPLY_FOR_JOB, applyJob);
}

export default function* getAllJobsSaga() {
    yield all([getAllJobsWatcher(), applyForJobWatcher()]);
}

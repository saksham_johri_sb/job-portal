import {
    GET_JOB_APPLICANTS,
    GET_JOB_APPLICANTS_FAILED,
    GET_JOB_APPLICANTS_SUCCESS,
} from '../../types';

export const getJobApplicants = payload => {
    return {
        type: GET_JOB_APPLICANTS,
        payload,
    };
};

export const getJobApplicantsSuccess = payload => {
    return {
        type: GET_JOB_APPLICANTS_SUCCESS,
        payload,
    };
};
export const getJobApplicantsFailed = payload => {
    return {
        type: GET_JOB_APPLICANTS_FAILED,
        payload,
    };
};

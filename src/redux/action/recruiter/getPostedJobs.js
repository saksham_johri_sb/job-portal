import {
    GET_POSTED_JOBS,
    GET_POSTED_JOBS_FAILED,
    GET_POSTED_JOBS_SUCCESS,
} from '../../types';

export const getPostedJobs = payload => {
    return {
        type: GET_POSTED_JOBS,
        payload,
    };
};

export const getPostedJobsSuccess = payload => {
    return {
        type: GET_POSTED_JOBS_SUCCESS,
        payload,
    };
};

export const getPostedJobsFailed = payload => {
    return {
        type: GET_POSTED_JOBS_FAILED,
        payload,
    };
};

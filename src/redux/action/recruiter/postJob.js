import { POST_JOB, POST_JOB_FAILED, POST_JOB_SUCCESS } from '../../types';

export const postJob = payload => {
    return {
        type: POST_JOB,
        payload,
    };
};

export const postJobSuccess = payload => {
    return {
        type: POST_JOB_SUCCESS,
        payload,
    };
};

export const postJobFailed = payload => {
    return {
        type: POST_JOB_FAILED,
        payload,
    };
};

import {
    SIGN_IN,
    SIGN_IN_SUCCESS,
    SIGN_IN_FAILED,
    SIGN_UP,
    SIGN_UP_SUCCESS,
    SIGN_UP_FAILED,
} from '../types';

export const signIn = payload => {
    return {
        type: SIGN_IN,
        payload,
    };
};

export const signUp = payload => {
    return {
        type: SIGN_UP,
        payload,
    };
};

export const signInSuccess = payload => {
    return {
        type: SIGN_IN_SUCCESS,
        payload,
    };
};

export const signInFailed = payload => {
    return {
        type: SIGN_IN_FAILED,
        payload,
    };
};
export const signUpSuccess = payload => {
    return {
        type: SIGN_UP_SUCCESS,
        payload,
    };
};

export const signUpFailed = payload => {
    return {
        type: SIGN_UP_FAILED,
        payload,
    };
};

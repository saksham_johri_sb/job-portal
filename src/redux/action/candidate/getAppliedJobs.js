import {
    GET_APPLIED_JOBS,
    GET_APPLIED_JOBS_FAILED,
    GET_APPLIED_JOBS_SUCCESS,
} from '../../types';

export const getAppliedJobs = payload => {
    return {
        type: GET_APPLIED_JOBS,
        payload,
    };
};

export const getAppliedJobsSuccess = payload => {
    return {
        type: GET_APPLIED_JOBS_SUCCESS,
        payload,
    };
};

export const getAppliedJobsFailed = payload => {
    return {
        type: GET_APPLIED_JOBS_FAILED,
        payload,
    };
};

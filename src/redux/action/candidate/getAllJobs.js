import {
    APPLY_FOR_JOB,
    GET_ALL_JOBS,
    GET_ALL_JOBS_FAILED,
    GET_ALL_JOBS_SUCCESS,
} from '../../types';

export const getAllJobs = payload => {
    return {
        type: GET_ALL_JOBS,
        payload,
    };
};

export const getAllJobsSuccess = payload => {
    return {
        type: GET_ALL_JOBS_SUCCESS,
        payload,
    };
};

export const getAllJobsFailed = payload => {
    return {
        type: GET_ALL_JOBS_FAILED,
        payload,
    };
};

export const applyForJob = payload => {
    return {
        type: APPLY_FOR_JOB,
        payload,
    };
};

import {
    FORGOT_PASSWORD,
    FORGOT_PASSWORD_SUCCESS,
    FORGOT_PASSWORD_FAILED,
    VERIFY_PASSWORD_TOKEN,
    VERIFY_PASSWORD_TOKEN_SUCCESS,
    VERIFY_PASSWORD_TOKEN_FAILED,
    CHANGE_PASSWORD,
    CHANGE_PASSWORD_SUCCESS,
    CHANGE_PASSWORD_FAILED,
} from '../types';

export const getPasswordToken = payload => {
    return {
        type: FORGOT_PASSWORD,
        payload,
    };
};

export const savePasswordToken = payload => {
    return {
        type: FORGOT_PASSWORD_SUCCESS,
        payload,
    };
};

export const getPasswordTokenFailed = payload => {
    return {
        type: FORGOT_PASSWORD_FAILED,
        payload,
    };
};

export const verifyPasswordToken = payload => {
    return {
        type: VERIFY_PASSWORD_TOKEN,
        payload,
    };
};

export const verifyPasswordTokenSuccess = payload => {
    return {
        type: VERIFY_PASSWORD_TOKEN_SUCCESS,
        payload,
    };
};

export const verifyPasswordTokenFailed = payload => {
    return {
        type: VERIFY_PASSWORD_TOKEN_FAILED,
        payload,
    };
};

export const changePassword = payload => {
    return {
        type: CHANGE_PASSWORD,
        payload,
    };
};

export const changePasswordSuccess = payload => {
    return {
        type: CHANGE_PASSWORD_SUCCESS,
        payload,
    };
};

export const changePasswordFailed = payload => {
    return {
        type: CHANGE_PASSWORD_FAILED,
        payload,
    };
};

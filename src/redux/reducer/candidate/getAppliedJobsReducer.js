import {
    GET_APPLIED_JOBS,
    GET_APPLIED_JOBS_FAILED,
    GET_APPLIED_JOBS_SUCCESS,
} from '../../types';

const initialState = {
    isLoading: false,
    jobs: [],
    error: null,
};

const getAppliedJobsReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case GET_APPLIED_JOBS:
            return {
                ...state,
                isLoading: true,
                error: null,
            };

        case GET_APPLIED_JOBS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                jobs: payload,
            };

        case GET_APPLIED_JOBS_FAILED:
            return {
                ...state,
                isLoading: false,
                error: payload,
            };

        default:
            return state;
    }
};

export default getAppliedJobsReducer;

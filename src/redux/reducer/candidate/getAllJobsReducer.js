import {
    APPLY_FOR_JOB,
    GET_ALL_JOBS,
    GET_ALL_JOBS_FAILED,
    GET_ALL_JOBS_SUCCESS,
} from '../../types';

const initialState = {
    isLoading: false,
    jobs: [],
    error: null,
};

const getAllJobsReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case GET_ALL_JOBS:
            return {
                ...state,
                isLoading: true,
                error: null,
            };

        case GET_ALL_JOBS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                jobs: payload,
            };

        case GET_ALL_JOBS_FAILED:
            return {
                ...state,
                isLoading: false,
                error: payload,
            };

        case APPLY_FOR_JOB:
            return {
                ...state,
                isLoading: true,
            };

        default:
            return state;
    }
};

export default getAllJobsReducer;

import { combineReducers } from 'redux';
import authReducer from './authReducer';
import getAllJobsReducer from './candidate/getAllJobsReducer';
import getAppliedJobsReducer from './candidate/getAppliedJobsReducer';
import forgotPasswordReducer from './forgotPaswordReducer';
import getJobAppicantsReducer from './recruiter/getJobAppicantsReducer';
import getPostedJobsReducer from './recruiter/getPostedJobsReducer';
import postJobReducer from './recruiter/postJobReducer';

const rootReducres = combineReducers({
    authentication: authReducer,
    forgotPassword: forgotPasswordReducer,
    postedJobs: getPostedJobsReducer,
    applicants: getJobAppicantsReducer,
    postJob: postJobReducer,
    allJobs: getAllJobsReducer,
    appliedJobs: getAppliedJobsReducer,
});

export default rootReducres;

import {
    SIGN_IN,
    SIGN_IN_FAILED,
    SIGN_IN_SUCCESS,
    SIGN_UP,
    SIGN_UP_FAILED,
    SIGN_UP_SUCCESS,
} from '../types';

const initialState = {
    isLoading: false,
    userData: {},
    signInError: null,
    signUpError: null,
};

const authReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case SIGN_IN:
            return {
                ...state,
                isLoading: true,
                signInError: null,
            };

        case SIGN_IN_SUCCESS:
            return {
                ...state,
                isLoading: false,
                userData: {
                    ...payload,
                },
            };

        case SIGN_IN_FAILED:
            return {
                ...state,
                isLoading: false,
                signInError: payload,
            };

        case SIGN_UP:
            return {
                ...state,
                isLoading: true,
                signUpError: null,
            };

        case SIGN_UP_SUCCESS:
            return {
                ...state,
                isLoading: false,
                userData: {
                    ...payload,
                },
            };

        case SIGN_UP_FAILED:
            return {
                ...state,
                isLoading: false,
                signUpError: payload,
            };
        default:
            return state;
    }
};

export default authReducer;

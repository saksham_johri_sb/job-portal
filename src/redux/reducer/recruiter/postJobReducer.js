import { POST_JOB, POST_JOB_FAILED, POST_JOB_SUCCESS } from '../../types';

const initialState = {
    isLoading: false,
    isSubmited: false,
    error: null,
};

const postJobReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case POST_JOB:
            return {
                ...state,
                isLoading: true,
                isSubmited: false,
                error: null,
            };

        case POST_JOB_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isSubmited: true,
            };

        case POST_JOB_FAILED:
            return {
                ...state,
                isLoading: false,
                error: payload,
            };

        default:
            return state;
    }
};

export default postJobReducer;

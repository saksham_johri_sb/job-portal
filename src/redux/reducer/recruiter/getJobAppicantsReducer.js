import {
    GET_JOB_APPLICANTS,
    GET_JOB_APPLICANTS_FAILED,
    GET_JOB_APPLICANTS_SUCCESS,
} from '../../types';

const initialState = {
    isLoading: false,
    jobId: null,
    applicants: [],
    error: null,
};

const getJobAppicantsReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case GET_JOB_APPLICANTS:
            return {
                ...state,
                isLoading: true,
                jobId: payload,
                applicants: [],
                error: null,
            };

        case GET_JOB_APPLICANTS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                applicants: payload,
            };

        case GET_JOB_APPLICANTS_FAILED:
            return {
                ...state,
                isLoading: false,
                error: payload,
            };

        default:
            return state;
    }
};

export default getJobAppicantsReducer;

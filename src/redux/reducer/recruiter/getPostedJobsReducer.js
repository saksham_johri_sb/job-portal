import {
    GET_POSTED_JOBS,
    GET_POSTED_JOBS_FAILED,
    GET_POSTED_JOBS_SUCCESS,
} from '../../types';

const initialState = {
    isLoading: false,
    postedJobs: [],
    error: null,
};

const getPostedJobsReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case GET_POSTED_JOBS:
            return {
                ...state,
                isLoading: true,
                error: null,
            };

        case GET_POSTED_JOBS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                postedJobs: payload,
            };

        case GET_POSTED_JOBS_FAILED:
            return {
                ...state,
                isLoading: false,
                error: payload,
            };

        default:
            return state;
    }
};

export default getPostedJobsReducer;

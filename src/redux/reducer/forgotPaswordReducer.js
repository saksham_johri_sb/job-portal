import {
    CHANGE_PASSWORD,
    CHANGE_PASSWORD_FAILED,
    CHANGE_PASSWORD_SUCCESS,
    FORGOT_PASSWORD,
    FORGOT_PASSWORD_FAILED,
    FORGOT_PASSWORD_SUCCESS,
    VERIFY_PASSWORD_TOKEN,
    VERIFY_PASSWORD_TOKEN_FAILED,
    VERIFY_PASSWORD_TOKEN_SUCCESS,
} from '../types';

const initialState = {
    isLoading: false,
    email: '',
    token: null,
    error: null,
    changePasswordError: null,
    isTokenVerified: false,
    isPasswordChanged: false,
};

const forgotPasswordReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case FORGOT_PASSWORD:
            return {
                ...state,
                isLoading: true,
                email: payload,
                error: null,
                isPasswordChanged: false,
                isTokenVerified: false,
                token: null,
            };

        case FORGOT_PASSWORD_SUCCESS:
            return {
                ...state,
                isLoading: false,
                token: payload,
                error: null,
            };

        case FORGOT_PASSWORD_FAILED:
            return {
                ...state,
                isLoading: false,
                token: null,
                error: payload,
            };

        case VERIFY_PASSWORD_TOKEN:
            return {
                ...state,
                isLoading: true,
                error: null,
                changePasswordError: null,
            };

        case VERIFY_PASSWORD_TOKEN_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isTokenVerified: true,
            };

        case VERIFY_PASSWORD_TOKEN_FAILED:
            return {
                ...state,
                isLoading: false,
                token: null,
                error: payload,
            };

        case CHANGE_PASSWORD:
            return {
                ...state,
                isLoading: true,
                isPasswordChanged: false,
                changePasswordError: null,
            };

        case CHANGE_PASSWORD_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isPasswordChanged: true,
                token: null,
            };

        case CHANGE_PASSWORD_FAILED:
            return {
                ...state,
                isLoading: false,
                changePasswordError: payload,
            };

        default:
            return state;
    }
};

export default forgotPasswordReducer;
